#Jason's Card Shuffler

##Requirement

Java 9+ for running jar as application
Maven for building application into a jar

##Installation

1. Clone the repository to a local machine
2. Navigate to procare-code-demo
3. Run command 'mvn clean install' in a terminal
4. Run command 'java -jar card-deck/target/card-deck.jar'

##Explanation
The code is developed with Javax Swing components as a standalone application. There are two ways to execute the application; double click on card-deck.jar, or run 'java -jar card-deck.jar' command. By doing so, the operating system should detect the JVM to load graphical user interface on computer screen. The application cannot be resized, and exit button would exit the application. This is essentially a basic application with two buttons one to reset the card deck and other to shuffle the deck. This application cannot be resized and exit button on top corner would exit the application. 

I have included two screenshots to illustrate the initial or reset state, and shuffled state. 

##Initial State
![Initial State](screenshot/InitialState.png)

##Shuffled State
![Shuffled State](screenshot/ShuffledState.png)

